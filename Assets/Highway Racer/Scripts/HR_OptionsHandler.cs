﻿//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

public class HR_OptionsHandler : MonoBehaviour {

	public GameObject pausedMenu;
	public GameObject pausedButtons;
	public GameObject optionsMenu;

	void OnEnable(){

		HR_GamePlayHandler.OnPaused += OnPaused;
		HR_GamePlayHandler.OnResumed += OnResumed;

	}

	public void ResumeGame () {
		
		HR_GamePlayHandler.Instance.Paused();
		
	}

	public void RestartGame () {

		HR_GamePlayHandler.Instance.RestartGame();
	
	}

	public void MainMenu () {
		
		HR_GamePlayHandler.Instance.MainMenu();
		
	}

	public void OptionsMenu (bool open) {

		if (open) {
			optionsMenu.SetActive (true);
			pausedButtons.SetActive (false);
		} else {
			optionsMenu.SetActive (false);
			pausedButtons.SetActive (true);
		}

	}

	void OnPaused () {
		
		pausedMenu.SetActive(true);
		pausedButtons.SetActive(true);

		AudioSource[] allSources = GameObject.FindObjectsOfType<AudioSource>();

		foreach(AudioSource source in allSources){
			if(source.transform.parent != HR_GamePlayHandler.Instance.transform)
				source.mute = true;
			else
				source.volume = .1f;
		}

		Time.timeScale = 0;
		
	}

	public void OnResumed () {

		pausedMenu.SetActive(false);
		pausedButtons.SetActive(false);

		Time.timeScale = 1;
		AudioSource[] allSources = GameObject.FindObjectsOfType<AudioSource>();

		foreach (AudioSource source in allSources) {
			if (source.transform.parent != HR_GamePlayHandler.Instance.transform)
				source.mute = false;
			else
				source.volume = .35f;

		}

	}

	public void ChangeCamera(){

		if (GameObject.FindObjectOfType<CarCamera> ())
			GameObject.FindObjectOfType<CarCamera> ().ChangeCamera ();

	}

	void OnDisable(){

		HR_GamePlayHandler.OnPaused -= OnPaused;
		HR_GamePlayHandler.OnResumed -= OnResumed;

	}

}
