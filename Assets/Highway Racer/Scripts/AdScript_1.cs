﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdScript_1 : MonoBehaviour
{
    //FZSM: Todo's
    //https://docs.unity3d.com/Manual/PlatformDependentCompilation.html
    //1-Show test interstitial in Editor_mode (if possible)
    //2-Create propietary Admob
    //3-Check compilation

    //Interstitial test , validated by Admob
    string InterstitialTest = "ca-app-pub-3940256099942544/1033173712";
    //Created for this app
    string InterstitialAd = "ca-app-pub-3654628576200837/4609018993";
    bool hasShownAdOneTime;

    // Use this for initialization
    void Start()
    {
        RequestInterstitialAds(); 
    }

    public void showInterstitialAd()
    {
        if (interstitial.IsLoaded()){
            Debug.Log("Interstitial fired");
            interstitial.Show();
        } else
        {
            Debug.Log("Interstitial not fired, not loaded");
        }
    }

    InterstitialAd interstitial;
    private void RequestInterstitialAds()
    {
        //This apps is working currently for Android

#if UNITY_EDITOR
        interstitial = new InterstitialAd(InterstitialTest);
#else
        interstitial = new InterstitialAd(InterstitialAd);
#endif

        // Initialize an InterstitialAd.

        //***Test***
        //AdRequest request = new AdRequest.Builder()
        //.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
        //.AddTestDevice("2077ef9a63d2b398840261c8221a0c9b")  // My test device.
        //.Build();
        //***Production***
        AdRequest request = new AdRequest.Builder().AddTestDevice(AdRequest.TestDeviceSimulator).Build();
        
        interstitial.OnAdClosed += Interstitial_OnAdClosed;  //Register Ad Close Event
        interstitial.LoadAd(request); // Load the interstitial with the request.
    }

    //Ad Close Event
    private void Interstitial_OnAdClosed(object sender, System.EventArgs e)
    {
        //Resume Play Sound
    }
	
    void Update()
    {
            if (!hasShownAdOneTime)
            {
                hasShownAdOneTime = true;
                Invoke("showInterstitialAd", 2.0f);
            }
    }
}