﻿//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

public class HR_LensFlare : MonoBehaviour {
	
	private Light parentLight;
	private LensFlare flare;
		
	void Awake () {
		
		parentLight = GetComponentInParent<Light>();
		flare = GetComponent<LensFlare>();

	}
	

	void Update () {
		
		flare.brightness = parentLight.intensity / 3f;

	}

}
