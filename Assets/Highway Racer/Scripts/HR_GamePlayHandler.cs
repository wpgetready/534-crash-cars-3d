﻿//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class HR_GamePlayHandler : MonoBehaviour {

	#region SINGLETON PATTERN
	public static HR_GamePlayHandler _instance;
	public static HR_GamePlayHandler Instance
	{
		get
		{
			if (_instance == null){
				_instance = GameObject.FindObjectOfType<HR_GamePlayHandler>();
			}
			
			return _instance;
		}
	}
	#endregion

	[Header("Time Of The Scene")]
	public DayOrNight dayOrNight;
	public enum DayOrNight{Day, Night, Rainy, Snowy}

	[Header("Current Mode")]
	internal Mode mode;
	internal enum Mode {OneWay, TwoWay, TimeAttack, Bomb}

	[Header("UI Canvases For GamePlay and GameOver")]
	public Canvas gameplayCanvas;
	public Canvas gameoverCanvas;

	[Header("Spawn Location Of The Cars")]
	public Transform spawnLocation;

	private GameObject player;

	private int selectedCarIndex = 0;
	private int selectedModeIndex = 0;
	private float minimumSpeed = 20f;

	internal bool gameStarted = false;
	internal bool paused = false;

	public int totalPlayedCount = 0;

	public delegate void onPlayerSpawned(GameObject p);
	public static event onPlayerSpawned OnPlayerSpawned;

	public delegate void onPlayerEnabled();
	public static event onPlayerEnabled OnPlayerEnabled;

	public delegate void onPlayerDisabled();
	public static event onPlayerDisabled OnPlayerDisabled;

	public delegate void onPlayerCrashed();
	public static event onPlayerCrashed OnPlayerCrashed;

	public delegate void onPaused();
	public static event onPaused OnPaused;

	public delegate void onResumed();
	public static event onResumed OnResumed;

	void Awake(){

		Time.timeScale = 1;
		AudioListener.volume = 0;

		if(HR_HighwayRacerProperties.Instance.gameplayClips != null && HR_HighwayRacerProperties.Instance.gameplayClips.Length > 0)
			RCC_CreateAudioSource.NewAudioSource(gameObject, "GamePlay Soundtrack", 0f, 0f, .35f, HR_HighwayRacerProperties.Instance.gameplayClips[UnityEngine.Random.Range(0, HR_HighwayRacerProperties.Instance.gameplayClips.Length)], true, true, false);

		selectedCarIndex = PlayerPrefs.GetInt("SelectedPlayerCarIndex");
		selectedModeIndex = PlayerPrefs.GetInt("SelectedModeIndex");
		totalPlayedCount = PlayerPrefs.GetInt("TotalPlayedCount");

		switch(selectedModeIndex){
		case 0:
			mode = Mode.OneWay;
			break;
		case 1:
			mode = Mode.TwoWay;
			break;
		case 2:
			mode = Mode.TimeAttack;
			break;
		case 3:
			mode = Mode.Bomb;
			break;
		}

	}

	void Start () {

		gameplayCanvas.enabled = true;
		gameoverCanvas.enabled = false;

		SpawnCar();
		StartCoroutine(WaitForGameStart());
	
	}

	IEnumerator WaitForGameStart(){

		yield return new WaitForSeconds(4);
		OnPlayerEnabled();
		gameStarted = true;

	}

	void Update(){

		if(AudioListener.volume < 1 && !paused && Time.timeSinceLevelLoad > .5f){
			
			AudioListener.volume = Mathf.MoveTowards(AudioListener.volume, 1f, Time.deltaTime);
			
		}

	}

	void SpawnCar () {

		if(mode != Mode.Bomb)
			player = (GameObject)Instantiate(HR_PlayerCars.Instance.cars[selectedCarIndex].playerCar);
		else
			player = (GameObject)Instantiate(HR_PlayerCars.Instance.bombedVehicleForBombMode);

		OnPlayerSpawned (player);
		OnPlayerDisabled();

		player.transform.position = spawnLocation.transform.position;
		player.transform.rotation = Quaternion.identity;

		player.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, minimumSpeed / 1.75f);

		if(dayOrNight == DayOrNight.Night || dayOrNight == DayOrNight.Rainy)
			player.GetComponent<RCC_CarControllerV3>().lowBeamHeadLightsOn = true;
	
	}

	public IEnumerator OnGameOver(float delayTime){
		
		OnPlayerCrashed ();

		gameplayCanvas.enabled = false;
		yield return new WaitForSeconds(delayTime);
		OnPaused ();
		//HR_Pause.Instance.Pause();
		gameoverCanvas.enabled = true;

		GameObject.FindObjectOfType<HR_Scoreboard>().DisplayResults();

		switch(mode){

		case Mode.OneWay:
			PlayerPrefs.SetInt("bestScoreOneWay", (int)player.GetComponent<HR_PlayerHandler>().score);
			break;
		case Mode.TwoWay:
			PlayerPrefs.SetInt("bestScoreTwoWay", (int)player.GetComponent<HR_PlayerHandler>().score);
			break;
		case Mode.TimeAttack:
			PlayerPrefs.SetInt("bestScoreTimeAttack", (int)player.GetComponent<HR_PlayerHandler>().score);
			break;
		case Mode.Bomb:
			PlayerPrefs.SetInt("bestScoreBomb", (int)player.GetComponent<HR_PlayerHandler>().score);
			break;

		}

		totalPlayedCount++;
		PlayerPrefs.SetInt("TotalPlayedCount", totalPlayedCount);

		
		
	}

	
	void OnLevelWasLoaded(){

		Time.timeScale = 1;
		AudioListener.pause = false;

	}

	public void MainMenu(){

		SceneManager.LoadScene(0);

	}

	public void RestartGame(){
		
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		
	}

	public void Paused(){

		paused = !paused;

		if(paused)
			OnPaused ();
		else
			OnResumed ();

	}

}
